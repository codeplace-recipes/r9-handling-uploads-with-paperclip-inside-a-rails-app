Rails.application.routes.draw do
  resources :messages, only: [:create, :destroy]
  resources :products
  root to: 'pages#home'
  get '/contact', to: 'messages#new', as: :contact
  get '/admin', to: 'pages#admin'

  get '/buy/:id', to: 'transactions#new', as: :show_buy
  post '/buy/:id', to: 'transactions#create', as: :buy
  get '/pickup/:guid', to: 'transactions#pickup', as: :pickup
  get '/download/:guid', to: 'transactions#download', as: :download
end
