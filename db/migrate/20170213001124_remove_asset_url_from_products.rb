class RemoveAssetUrlFromProducts < ActiveRecord::Migration[5.0]
  def change
    remove_column :products, :asset_url
  end
end
