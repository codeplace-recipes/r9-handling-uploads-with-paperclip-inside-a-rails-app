class AddSalesCountToProducts < ActiveRecord::Migration[5.0]
  def change
    change_table :products do |t|
      t.integer :sales_count, default: 0
    end

    reversible do |dir|
      dir.up { data }
    end
  end

  def data
    execute <<-SQL.squish
      UPDATE products
         SET sales_count = (SELECT count(1) FROM sales WHERE sales.product_id = products.id)
    SQL
  end
end
