Product.create(
  [
    {
      title: 'Imagine what you could build if you learned Ruby on Rails...',
      description: 'Learning to build a modern web application is daunting. Ruby on Rails makes it much easier and more fun.',
      price: 200.00
    },
    {
      title: 'Learn by building - HTML5, CSS3, JavaScript / jQuery',
      description: 'Becoming a junior web developer is impossible without learning about basic blocks of every website out there.',
      price: 120.00
    },
    {
      title: 'Ember JS - A framework for creating ambitious web applications.',
      description: 'Ember.js allows developers to create scalable single-page web applications by incorporating common idioms and best practices into the framework.',
      price: 130.00
    },
    {
      title: 'Angular 2 - One framework. Mobile & desktop.',
      description: 'Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target.',
      price: 145.00
    },
    {
      title: 'React - A JavaScript library for building user interfaces',
      description: 'React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.',
      price: 150.99
    },
    {
      title: 'Ionic - The top open source framework for building amazing mobile apps.',
      description: 'Ionic Framework offers the best web and native app components for building highly interactive native and progressive web apps with Angular.',
      price: 135.45
    },
    {
      title: 'Bootstrap 4 - the most popular HTML, CSS, and JS framework in the world.',
      description: 'Bootstrap is made for building responsive, mobile-first projects and makes front-end web development faster and easier.',
      price: 100.55
    },
    {
      title: 'Node JS - Designed to build scalable network applications.',
      description: "Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, npm, is the largest ecosystem of open source libraries in the world",
      price: 135.45
    },
    {
      title: 'Sinatra - DSL for quickly creating web applications in Ruby.',
      description: 'Sinatra does not follow the typical model–view–controller pattern but focuses on quickly creating web-applications with minimal effort.',
      price: 125.45
    },

    {
      title: 'Django - The web framework for perfectionists with deadlines.',
      description: 'Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design.',
      price: 155.45
    }
  ]
)
