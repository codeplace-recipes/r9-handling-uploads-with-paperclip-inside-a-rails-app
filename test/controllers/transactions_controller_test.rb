require 'test_helper'

class TransactionsControllerTest < ActionDispatch::IntegrationTest
  test "should get pickup" do
    get transactions_pickup_url
    assert_response :success
  end

  test "should get create" do
    get transactions_create_url
    assert_response :success
  end

  test "should get download" do
    get transactions_download_url
    assert_response :success
  end

end
