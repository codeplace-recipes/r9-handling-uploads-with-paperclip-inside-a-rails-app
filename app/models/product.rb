class Product < ApplicationRecord
  validates :title, presence: true
  validates :price, presence: true
  validates_numericality_of :price, greater_than: 0.49, message: 'must be at least 50 cents'
  has_attached_file :photo, styles: { large: '800x500#', thumb: '600x375#' },
                            default_url: '/images/:style/placeholder.png'
  validates_attachment_content_type :photo, content_type: %r{\Aimage\/.*\z}
  validates_attachment_size :photo, less_than: 5.megabytes
  has_attached_file :asset
  validates_attachment :asset, content_type: { content_type: 'application/pdf' }
  has_many :sales

  scope :latest, -> { last(4).reverse }
end
