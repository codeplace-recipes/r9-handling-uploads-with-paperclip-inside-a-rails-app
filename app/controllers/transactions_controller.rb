class TransactionsController < ApplicationController
  def pickup
    @sale = Sale.find_by!(guid: params[:guid])
    @product = @sale.product
  end

  def create
    product = Product.find(params[:id])
    token = params[:stripeToken]

    begin
      Stripe::Charge.create(amount: (product.price * 100).to_i, currency: 'usd', card: token, description: params[:email])
      @sale = product.sales.create!(email: params[:email])
      redirect_to pickup_url(guid: @sale.guid)
    rescue Stripe::CardError => e
      @error = e
      render :new
    end
  end

  def download
    @sale = Sale.find_by!(guid: params[:guid])
    redirect_to @sale.product.asset.url
  end
end
