class ProductsController < ApplicationController
  http_basic_authenticate_with name: 'admin', password: 'secret', except: [:index, :show]
  before_action :set_product, except: [:index, :create, :new]

  def index
    @products = Product.all
  end

  def show; end

  def new
    @product = Product.new
  end

  def edit; end

  def create
    @product = Product.new(product_params)
    respond_to do |format|
      format.html { redirect_to product_path @product, notice: 'Product was successfully created.' } if @product.save
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to product_path @product, notice: 'Product was successfully updated.' }
      end
    end
  end

  def destroy
    respond_to do |format|
      format.html { redirect_to products_path, notice: 'Product was successfully deleted.' } if @product.destroy
    end
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:title, :description, :price, :asset, :photo)
  end
end
